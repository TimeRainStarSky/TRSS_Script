---
home: true
icon: home
title: 项目主页
heroAlt: 苏半夏
heroImage: /苏半夏.png
heroImageDark: /苏半夏D.png
heroText: TRSS Script
tagline: TRSS 脚本 使用文档
actions:
  - text: 安装教程 ️📲
    link: Install/
    type: primary

  - text: 使用教程 💡
    link: Guide/

features:
  - title: 操作简单
    icon: markdown
    details: 一键 安装、启动、管理 项目

  - title: 容器化
    icon: slides
    details: 减少环境问题，避免文件残留，快速删库跑路

  - title: Dialog 交互式对话框
    icon: layout
    details: 通过文本实现交互窗口展示内容，无需图形界面，系统资源占用低

  - title: tmux 终端会话管理
    icon: blog
    details: 一个终端 管理多个会话，分屏 同时处理多个操作，多人连接 远程演示
    link: https://github.com/tmux/tmux

  - title: micro 文本编辑
    icon: editor
    details: 易于使用的 文本编辑器
    link: https://micro-editor.github.io

  - title: ranger 文件管理
    icon: folder
    details: 易于使用的 文件管理器
    link: https://ranger.github.io

  - title: tar.zst 备份管理
    icon: info
    details: 一键 创建、恢复、传输 备份
    link: https://facebook.github.io/zstd

  - title: fish 交互式 Shell
    icon: palette
    details: 易于使用的 Shell
    link: https://fishshell.com

  - title: 阿里云盘 CLI
    icon: contrast
    details: 一键 上传、下载、管理 阿里云盘 文件
    link: https://github.com/tickstep/aliyunpan

  - title: 百度网盘 CLI
    icon: support
    details: 一键 上传、下载、管理 百度网盘 文件
    link: https://github.com/qjfoidnh/BaiduPCS-Go

  - title: btop 资源监视
    icon: pic
    details: 直观显示 系统资源使用情况
    link: https://github.com/aristocratos/btop

  - title: htop 进程管理
    icon: config
    details: 易于使用的 进程管理器
    link: https://htop.dev

  - title: NetHogs 实时网速
    icon: rss
    details: 直观显示 实时网速情况
    link: https://github.com/raboof/nethogs

  - title: Ncdu 存储分析
    icon: storage
    details: 直观显示 存储占用情况
    link: https://dev.yorhel.nl/ncdu

  - title: ripgrep 文本搜索
    icon: sitemap
    details: 快速文本搜索工具
    link: https://github.com/BurntSushi/ripgrep

  - title: fd 文件搜索
    icon: copy
    details: 快速文件搜索工具
    link: https://github.com/sharkdp/fd

  - title: fzf 模糊搜索
    icon: search
    details: 快速模糊搜索工具
    link: https://github.com/junegunn/fzf

  - title: 🌈 lolcat 彩虹输出
    details: 彩虹颜色字体生成器
    link: https://github.com/busyloop/lolcat

  - title: 其他功能
    icon: update
    details: 自启动管理、插件管理、滚动更新 等等

  - title: 更多功能
    icon: more
    details: 等待你来提出……
---

## 联系方式

- QQ 群组：
1. [659945190](https://jq.qq.com/?k=VBuHGPv3)
2. [1027131254](https://jq.qq.com/?k=Af0pTDHU)
3. [300714227](https://jq.qq.com/?k=V2xVpaR7)

### 时雨🌌星空

- QQ：[2536554304](https://qm.qq.com/cgi-bin/qm/qr?k=x8LtlP8vwZs7qLwmsbCsyLoAHy7Et1Pj)
- GitHub：[TimeRainStarSky](https://github.com/TimeRainStarSky)
- Telegram：[TimeRainStarSky](https://t.me/TimeRainStarSky)

## 感谢支持

- [火柴](https://gitee.com/ranks)
- [椰羊](https://gitee.com/yeyang52)
- [听语惊花](https://gitee.com/Nwflower)
- [SnowyKami](https://gitee.com/snowykami)
- [花海里的秋刀鱼](https://gitee.com/Saury-loser)

## 赞助支持

- [爱发电](https://afdian.net/a/TimeRainStarSky)
- [Partme](https://partme.com/TimeRainStarSky)
- [感谢名单](https://github.com/TimeRainStarSky/SponsorList)