---
title: 安装教程
icon: config
date: 2022-12-14
category:
  - 安装教程
---

- [<FontIcon icon="windows"/> Windows](Windows.md)

- [<FontIcon icon="discover"/> Mac](Mac.md)

- [<FontIcon icon="linux"/> Linux](Linux.md)

- [<FontIcon icon="android"/> Android](Android.md)