---
title: MT管理器
icon: page
date: 2022-12-14
category:
  - 安装教程
  - Android
  - MT管理器
---

1. 准备：[MT 管理器](https://mt2.cn) 或 [Termux](https://termux.dev/cn)

2. 打开 `MT管理器`，菜单选择 `终端模拟器`，执行：

```sh
curl -LO gitee.com/TimeRainStarSky/TRSS-MTArch/raw/main/Install.sh && bash Install.sh
```

---

启动：`start`

文件管理：点击 `顶栏 跳转` 输入：

```
/data/user/0/bin.mt.plus/home/Arch/rootfs/root
```

<BiliBili bvid="BV1J84y1H71b" ratio="9:20"/>