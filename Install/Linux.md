---
title: Linux
icon: linux
order: 3
date: 2022-12-14
category:
  - 安装教程
  - Linux
---

- [<FontIcon icon="page"/> Docker：应用容器（推荐）](Docker.md)

- [<FontIcon icon="page"/> TMOE：PRoot/chroot 容器（不推荐）](TMOE.md)