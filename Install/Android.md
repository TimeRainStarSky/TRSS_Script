---
title: Android
icon: android
order: 4
date: 2022-12-14
category:
  - 安装教程
  - Android
---

- [<FontIcon icon="page"/> MT 管理器 / Termux：PRoot 容器（无 root 推荐）](MTArch.md)

- [<FontIcon icon="page"/> TMOE：PRoot/chroot 容器（有 root 推荐）](TMOE.md)

- [<FontIcon icon="page"/> vmConsole：QEMU 虚拟机（不推荐）](vmConsole.md)