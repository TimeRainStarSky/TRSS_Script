---
title: Mac
icon: discover
order: 2
date: 2022-12-14
category:
  - 安装教程
  - Mac
---

- [<FontIcon icon="page"/> Docker：Linux 应用容器](Docker.md)

::: warning

可能出现找不到 md5sum 的问题，按以下示例禁用 md5 校验：

```sh
# 原安装命令
bash <(x)
# 修改后安装命令
bash <(x|sed /md5sum/d)
```

:::