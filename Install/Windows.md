---
title: Windows
icon: windows
order: 1
date: 2022-12-14
category:
  - 安装教程
  - Windows
---

- [<FontIcon icon="page"/> MSYS2：Linux 兼容层（关闭 虚拟化 或 内存<4G 推荐）](MSYS2.md)

- [<FontIcon icon="page"/> ArchWSL：Linux 子系统（开启 虚拟化 且 内存>8G 推荐）](ArchWSL.md)

- [<FontIcon icon="page"/> Docker：Linux 应用容器（不推荐）](Docker.md)

::: tip
查看虚拟化启用状态：任务管理器➡性能➡CPU
:::