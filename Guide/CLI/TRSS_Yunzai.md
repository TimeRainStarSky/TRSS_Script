---
title: TRSS Yunzai CLI 使用教程
icon: creative
order: 4
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS Yunzai
---

## 主功能

- 用法：`tsyz [用法]`

| 名称                  | 用法     | 缩写     | 说明                 |
| --------------------- | -------- | -------- | -------------------- |
| Yunzai                | `Yunzai` | `y` `yz` | 启动 Yunzai 项目界面 |
| Adachi                | `Adachi` | `a` `ac` | 启动 Adachi 项目界面 |
| [插件管理](#插件管理) | `plugin` | `p`      | 启动 插件管理 界面   |

### 插件管理

- 用法：`tsyz plugin [用法]`

| 名称             | 用法        | 缩写      | 说明                       |
| ---------------- | ----------- | --------- | -------------------------- |
| JS 插件管理      | `js`        | `j`       | 启动 JS 插件管理 界面      |
| Git 插件管理     | `git`       | `g`       | 启动 Git 插件管理 界面     |
| Adachi 插件管理  | `Adachi`    | `a` `ac`  | 启动 Adachi 插件管理 界面  |
| pnpm 软件包管理  | `pnpm`      | `p`       | 启动 pnpm 软件包管理 界面  |
| Dragonfly 数据库 | `Dragonfly` | `d` `dgf` | 启动 Dragonfly 数据库 界面 |