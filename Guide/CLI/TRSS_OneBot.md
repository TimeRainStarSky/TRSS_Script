---
title: TRSS OneBot CLI 使用教程
icon: creative
order: 3
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS OneBot
---

## 主功能

- 用法：`tsob [用法]`

| 名称       | 用法         | 缩写      | 说明                         |
| ---------- | ------------ | --------- | ---------------------------- |
| go-cqhttp  | `go-cqhttp`  | `g` `gcq` | 启动 go-cqhttp 项目界面      |
| ZeroBot    | `ZeroBot`    | `z` `zbp` | 启动 ZeroBot-Plugin 项目界面 |
| PaimengBot | `PaimengBot` | `p` `pmb` | 启动 PaimengBot 项目界面     |