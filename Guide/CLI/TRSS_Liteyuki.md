---
title: TRSS Liteyuki CLI 使用教程
icon: creative
order: 3
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS Liteyuki
---

## 主功能

- 用法：`tsly [用法]`

| 名称         | 用法           | 缩写      | 说明                       |
| ------------ | -------------- | --------- | -------------------------- |
| go-cqhttp    | `go-cqhttp`    | `g` `gcq` | 启动 go-cqhttp 项目界面    |
| Liteyuki     | `Liteyuki`     | `l` `ly`  | 启动 Liteyuki 项目界面     |
| LittlePaimon | `LittlePaimon` | `lp`      | 启动 LittlePaimon 项目界面 |

### 通用功能

- 用法：`tsly [项目名称] [用法]`

| 名称     | 用法     | 缩写 | 说明               |
| -------- | -------- | ---- | ------------------ |
| 插件管理 | `plugin` | `p`  | 启动 插件管理 界面 |