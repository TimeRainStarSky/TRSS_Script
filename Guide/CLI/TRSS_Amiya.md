---
title: TRSS Amiya CLI 使用教程
icon: creative
order: 6
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS Amiya
---

## 主功能

- 用法：`tsai [用法]`

| 名称                  | 用法        | 缩写      | 说明                               |
| --------------------- | ----------- | --------- | ---------------------------------- |
| Amiya                 | `Amiya`     | `a` `ai`  | 启动 Sagiri 项目界面               |
| Mirai                 | `Mirai`     | `m` `mcl` | 启动 Mirai Console Loader 项目界面 |
| go-cqhttp             | `go-cqhttp` | `g` `gcq` | 启动 go-cqhttp 项目界面            |
| [插件管理](#插件管理) | `plugin`    | `p`       | 启动 插件管理 界面                 |

### 插件管理

- 用法：`tsai plugin [用法]`

| 名称            | 用法   | 缩写 | 说明                      |
| --------------- | ------ | ---- | ------------------------- |
| PyPI 软件包管理 | `pypi` | `p`  | 启动 PyPI 软件包管理 界面 |
| Poetry fish     | `fish` | `f`  | 启动 Poetry fish          |