---
title: TRSS Sagiri CLI 使用教程
icon: creative
order: 5
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS Sagiri
---

## 主功能

- 用法：`tssi [用法]`

| 名称                  | 用法      | 缩写       | 说明                               |
| --------------------- | --------- | ---------- | ---------------------------------- |
| Mirai                 | `Mirai`   | `m` `mcl`  | 启动 Mirai Console Loader 项目界面 |
| Sagiri                | `Sagiri`  | `s` `si`   | 启动 Sagiri 项目界面               |
| Sagiri3               | `Sagiri3` | `s3` `si3` | 启动 Sagiri3 项目界面              |
| [插件管理](#插件管理) | `plugin`  | `p`        | 启动 插件管理 界面                 |

### 插件管理

- 用法：`tssi plugin [用法]`

| 名称            | 用法   | 缩写 | 说明                      |
| --------------- | ------ | ---- | ------------------------- |
| PyPI 软件包管理 | `pypi` | `p`  | 启动 PyPI 软件包管理 界面 |
| Saya 插件管理   | `saya` | `s`  | 启动 Saya 插件管理 界面   |
| Poetry fish     | `fish` | `f`  | 启动 Poetry fish          |