---
title: TRSS Zhenxun CLI 使用教程
icon: creative
order: 7
date: 2022-12-18
category:
  - 使用教程
  - CLI
  - TRSS Zhenxun
---

## 主功能

- 用法：`tszx [用法]`

| 名称                  | 用法        | 缩写      | 说明                    |
| --------------------- | ----------- | --------- | ----------------------- |
| go-cqhttp             | `go-cqhttp` | `g` `gcq` | 启动 go-cqhttp 项目界面 |
| Zhenxun               | `Zhenxun`   | `z` `zx`  | 启动 Zhenxun 项目界面   |
| WebUI                 | `WebUI`     | `w`       | 启动 WebUI 项目界面     |
| [插件管理](#插件管理) | `plugin`    | `p`       | 启动 插件管理 界面      |

### 插件管理

- 用法：`tszx plugin [用法]`

| 名称            | 用法   | 缩写 | 说明                      |
| --------------- | ------ | ---- | ------------------------- |
| PyPI 软件包管理 | `pypi` | `p`  | 启动 PyPI 软件包管理 界面 |
| Git 插件管理    | `git`  | `g`  | 启动 Git 插件管理 界面    |
| Poetry fish     | `fish` | `f`  | 启动 Poetry fish          |