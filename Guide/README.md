---
title: 使用教程
icon: guide
date: 2022-12-18
category:
  - 使用教程
---

## 脚本功能

- [<FontIcon icon="shell"/> CLI：命令行界面](CLI/)

- [<FontIcon icon="window"/> TUI：文本用户界面](TUI/)

- [<FontIcon icon="command"/> 常用命令](Command.md)

## 项目功能

- [<FontIcon icon="creative"/> go-cqhttp](Project/go-cqhttp.md)

- [<FontIcon icon="creative"/> Mirai](Project/Mirai.md)

- [<FontIcon icon="creative"/> ZeroBot](Project/ZeroBot.md)

- [<FontIcon icon="creative"/> PaimengBot](Project/PaimengBot.md)

- [<FontIcon icon="creative"/> Liteyuki](Project/Liteyuki.md)

- [<FontIcon icon="creative"/> LittlePaimon](Project/LittlePaimon.md)

- [<FontIcon icon="creative"/> Yunzai](Project/Yunzai.md)

- [<FontIcon icon="creative"/> TRSS-Yunzai](Project/TRSS-Yunzai.md)

- [<FontIcon icon="creative"/> Adachi](Project/Adachi.md)

- [<FontIcon icon="creative"/> Sagiri](Project/Sagiri.md)

- [<FontIcon icon="creative"/> Amiya](Project/Amiya.md)

- [<FontIcon icon="creative"/> Zhenxun](Project/Zhenxun.md)