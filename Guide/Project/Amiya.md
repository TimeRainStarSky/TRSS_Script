---
title: Amiya
icon: creative
order: 11
date: 2023-04-04
category:
  - 使用教程
  - Amiya
---

## 关于

- [AmiyaBot 文档](https://www.amiyabot.com)
- [AmiyaBot 源代码存储库](https://github.com/AmiyaBot)