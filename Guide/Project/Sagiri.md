---
title: Sagiri
icon: creative
order: 10
date: 2023-04-04
category:
  - 使用教程
  - Sagiri
---

## 关于

- [SAGIRI-BOT 文档](https://sagiri-kawaii.github.io/sagiri-bot)
- [SAGIRI-BOT 源代码存储库](https://github.com/SAGIRI-kawaii/sagiri-bot)