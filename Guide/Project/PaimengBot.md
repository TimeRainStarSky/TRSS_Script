---
title: PaimengBot
icon: creative
order: 4
date: 2023-04-04
category:
  - 使用教程
  - PaimengBot
---

## 关于

- [ZeroBot](https://github.com/wdvxdr1123/ZeroBot)
- [派蒙Bot 文档](https://richeyjang.github.io/PaimengBot)
- [派蒙Bot 源代码存储库](https://github.com/RicheyJang/PaimengBot)