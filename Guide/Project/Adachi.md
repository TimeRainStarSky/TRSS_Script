---
title: Adachi
icon: creative
order: 9
date: 2023-04-04
category:
  - 使用教程
  - Adachi
---

## 关于

- [Adachi-BOT 文档](https://docs.adachi.top)
- [Adachi-BOT 源代码存储库](https://github.com/SilveryStar/Adachi-BOT)