---
title: LittlePaimon
icon: creative
order: 6
date: 2023-04-04
category:
  - 使用教程
  - LittlePaimon
---

## 关于

- [NoneBot2 文档](https://v2.nonebot.dev)
- [NoneBot2 源代码存储库](https://github.com/nonebot/nonebot2)
- [小派蒙 文档](https://docs.paimon.cherishmoon.fun)
- [小派蒙 源代码存储库](https://github.com/CMHopeSunshine/LittlePaimon)