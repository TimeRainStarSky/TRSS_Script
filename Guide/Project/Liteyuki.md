---
title: Liteyuki
icon: creative
order: 5
date: 2023-04-04
category:
  - 使用教程
  - Liteyuki
---

## 关于

- [NoneBot2 文档](https://v2.nonebot.dev)
- [NoneBot2 源代码存储库](https://github.com/nonebot/nonebot2)
- [轻雪机器人](https://gitee.com/snowykami/liteyuki-bot)