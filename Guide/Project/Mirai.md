---
title: Mirai
icon: creative
order: 2
date: 2023-04-04
category:
  - 使用教程
  - Mirai
---

## 关于

- [MiraiForum 论坛](https://mirai.mamoe.net)
- [Mirai 开发文档](https://docs.mirai.mamoe.net)
- [Mirai 源代码存储库](https://github.com/mamoe/mirai)
- [Mirai Console Loader](https://github.com/iTXTech/mirai-console-loader)