---
title: Zhenxun
icon: creative
order: 12
date: 2023-04-04
category:
  - 使用教程
  - Zhenxun
---

## 关于

- [NoneBot2 文档](https://v2.nonebot.dev)
- [NoneBot2 源代码存储库](https://github.com/nonebot/nonebot2)
- [绪山真寻Bot 文档](https://hibikier.github.io/zhenxun_bot)
- [绪山真寻Bot 源代码存储库](https://github.com/HibiKier/zhenxun_bot)