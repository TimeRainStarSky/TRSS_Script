---
title: 常用命令
icon: command
date: 2023-02-20
category:
  - 使用教程
  - 命令
---

## tmux 终端会话管理

| 名称         | 用法       |
| ------------ | ---------- |
| 离开窗口     | Ctrl+B D   |
| 切换窗口     | Ctrl+B W   |
| 新建窗口     | Ctrl+B C   |
| 分屏         | Ctrl+B -/_ |
| 切换鼠标模式 | 按住 Shift |
| 切换复制模式 | Ctrl+B ↲   |
| 杀死窗口     | Ctrl+B X Y |

## micro 文本编辑

| 名称 | 用法   | 备注              |
| ---- | ------ | ----------------- |
| 打开 | Ctrl+O |
| 保存 | Ctrl+S |
| 全选 | Ctrl+A |
| 粘贴 | Ctrl+V |
| 复制 | Ctrl+C | 复制整行 Ctrl+D   |
| 剪切 | Ctrl+X | 剪切整行 Ctrl+K   |
| 撤销 | Ctrl+Z | 恢复 Ctrl+Y       |
| 查找 | Ctrl+F | ⬇ Ctrl+N ⬆ Ctrl+P |
| 命令 | Ctrl+E | Shell 命令 Ctrl+B |
| 录制 | Ctrl+U | 播放录制 Ctrl+J   |
| 退出 | Ctrl+Q | 保存 Y 不保存 N   |
| 帮助 | Ctrl+G |

## Ncdu 存储分析

| 名称 | 用法 |
| ---- | ---- |
| 删除 | D    |

## 其它命令

| 名称 | 用法 |
| ---- | ---- |
| 退出 | Q    |